FROM python:3.8.6-slim

ARG DAGSTER_VERSION=0.12.10

RUN apt-get update -yqq && \
    apt-get install -yqq cron
    
RUN pip install \
    dagster==${DAGSTER_VERSION} \
    dagster-graphql==${DAGSTER_VERSION} \
    dagster-postgres==${DAGSTER_VERSION} \
    dagster-celery[flower,redis,kubernetes]==${DAGSTER_VERSION} \
    dagster-aws==${DAGSTER_VERSION} \
    dagster-k8s==${DAGSTER_VERSION} \
    dagster-celery-k8s==${DAGSTER_VERSION} \
    dagit==${DAGSTER_VERSION}

RUN mkdir -p /home/orchestrator
RUN mkdir -p /opt/dagster/app 

COPY ./Dockerfile /opt/dagster/app 
COPY ./example_project  /opt/dagster/app
#WORKDIR /home/orchestrator
