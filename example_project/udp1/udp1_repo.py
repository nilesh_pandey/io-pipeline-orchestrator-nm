from dagster import solid, pipeline, repository, ScheduleDefinition
import time
import requests

@solid
def delay_5(context):
    context.log.info("============== ", time.sleep(5) ," =============")
@solid
def delay_10(context):
    context.log.info("============== ", time.sleep(10) ," =============")
@solid
def delay_20(context):
    context.log.info("============== ", time.sleep(20) ," =============")
@solid
def delay_30(context):
    context.log.info("============== ", time.sleep(30) ," =============")
@solid
def delay_60(context):
    context.log.info("============== ", time.sleep(60) ," =============")

@pipeline
def udp1_pipeline1():
    delay_5()

@pipeline
def udp1_pipeline2():
    delay_10()

@pipeline
def udp1_pipeline3():
     delay_20()

@pipeline
def udp1_pipeline4():
     delay_30()

@pipeline
def udp1_pipeline5():
     delay_60()

@pipeline
def udp1_pipeline6():
    delay_5()

@pipeline
def udp1_pipeline7():
    delay_5()

@pipeline
def udp1_pipeline8():
    delay_5()  

@pipeline
def udp1_pipeline9():
    delay_5()          

@pipeline
def udp1_pipeline10():
    delay_5()

@pipeline
def udp1_pipeline11():
    delay_5()

@pipeline
def udp1_pipeline12():
    delay_5()    

@pipeline
def udp1_pipeline13():
    delay_5()

@pipeline
def udp1_pipeline14():
    delay_5()    

@pipeline
def udp1_pipeline15():
    delay_5()    

@pipeline
def udp1_pipeline16():
    delay_5()    

@pipeline
def udp1_pipeline17():
    delay_5()    

@pipeline
def udp1_pipeline18():
    delay_5()    

@pipeline
def udp1_pipeline19():
    delay_5()    

@pipeline
def udp1_pipeline20():
    delay_5()    

@pipeline
def udp1_pipeline21():
    delay_5()

@pipeline
def udp1_pipeline22():
    delay_5()    

@pipeline
def udp1_pipeline23():
    delay_5()

@pipeline
def udp1_pipeline24():
    delay_5()

@pipeline
def udp1_pipeline25():
    delay_5()

@pipeline
def udp1_pipeline26():
    delay_5()

@pipeline
def udp1_pipeline27():
    delay_5()

@pipeline
def udp1_pipeline28():
    delay_5()

@pipeline
def udp1_pipeline29():
    delay_5()

@pipeline
def udp1_pipeline30():
    delay_5()

@pipeline
def udp1_pipeline31():
    delay_5()    

@pipeline
def udp1_pipeline32():
    delay_5()

@pipeline
def udp1_pipeline33():
    delay_5()    

@pipeline
def udp1_pipeline34():
    delay_5()

@pipeline
def udp1_pipeline35():
    delay_5()

@pipeline
def udp1_pipeline36():
    delay_5()

@pipeline
def udp1_pipeline37():
    delay_5()

@pipeline
def udp1_pipeline38():
    delay_5()

@pipeline
def udp1_pipeline39():
    delay_5()    

@pipeline
def udp1_pipeline40():
    delay_5()    

@pipeline
def udp1_pipeline41():
    delay_5()   

@pipeline
def udp1_pipeline42():
    delay_5()

@pipeline
def udp1_pipeline43():
    delay_5()

@pipeline
def udp1_pipeline44():
    delay_5()

@pipeline
def udp1_pipeline45():
    delay_5()

@pipeline
def udp1_pipeline46():
    delay_5()    

@pipeline
def udp1_pipeline47():
    delay_5()    

@pipeline
def udp1_pipeline48():
    delay_5()    

@pipeline
def udp1_pipeline49():
    delay_5()    

@pipeline
def udp1_pipeline50():
    delay_5()    




def pipelines_with_schedules():
    pipeline_template_name = 'udp1_pipeline'
    pipeline_and_schedules = []
    for pipeline_number in range(1,51):
        pipeline_and_schedules.append(globals()[f'{pipeline_template_name}{pipeline_number}'])
        pipeline_and_schedules.append(ScheduleDefinition(name=f"schedule{pipeline_number}",
                                                        pipeline_name= f"udp1_pipeline{pipeline_number}",
                                                        cron_schedule="*/1 * * * *")
                                                        )
    return pipeline_and_schedules    

@repository
def udp1_repo():
    return pipelines_with_schedules()

