from dagster import PipelineDefinition, repository
from dagster.core.definitions import solid
import yaml, os
from pathlib import Path

class UniqueKeyLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = []
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            assert key not in mapping
            mapping.append(key)
        return super().construct_mapping(node, deep)


def load_yaml_from_path(yaml_file):
    with open(yaml_file , 'r') as f:
        data = yaml.load(f, Loader=UniqueKeyLoader)
    return data


@solid
def solid1(context):
    return context.log.info('solid1 output')

def construct_pipeline_with_yaml(yaml_file):
    yaml_data = load_yaml_from_path(yaml_file)
    solid_defs = []
    pipeline_name = yaml_data['pipeline']['name']
    cron_schedule = yaml_data['pipeline']['cron_schedule']
    if yaml_data['pipeline']['solid'] == 'solid1':
        solid_defs.append(solid1)

    return PipelineDefinition(name=pipeline_name, solid_defs=solid_defs)


def my_pipeline():
    pipeline_path = '/home/orchestrator/udp0/pipelines'
    pipelines = os.listdir(pipeline_path)
    pipeline_defs = []
    for pipeline_yaml in pipelines:
        pipeline_defs.append(construct_pipeline_with_yaml(f'{pipeline_path}/{pipeline_yaml}'))
    return pipeline_defs


@repository
def udp0_repo():
    return my_pipeline()

