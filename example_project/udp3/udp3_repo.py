from dagster import solid, pipeline, repository, ScheduleDefinition
import time

@solid
def delay_5(context):
    context.log.info("==============", time.sleep(5) ,"=============")
    
@solid
def delay_10(context):
    context.log.info("============== ", time.sleep(10) ," =============")
@solid
def delay_20(context):
    context.log.info("============== ", time.sleep(20) ," =============")
@solid
def delay_30(context):
    context.log.info("============== ", time.sleep(30) ," =============")
@solid
def delay_60(context):
    context.log.info("============== ", time.sleep(60) ," =============")


@pipeline
def udp3_pipeline1():
    delay_5()
@pipeline
def udp3_pipeline2():
    delay_10()
@pipeline
def udp3_pipeline3():
    delay_20()
@pipeline
def udp3_pipeline4():
    delay_30()
@pipeline
def udp3_pipeline5():
    delay_60()
@pipeline
def udp3_pipeline6():
    delay_5()

@pipeline
def udp3_pipeline7():
    delay_5()

@pipeline
def udp3_pipeline8():
    delay_5()  

@pipeline
def udp3_pipeline9():
    delay_5()          

@pipeline
def udp3_pipeline10():
    delay_5()

@pipeline
def udp3_pipeline11():
    delay_5()

@pipeline
def udp3_pipeline12():
    delay_5()    

@pipeline
def udp3_pipeline13():
    delay_5()

@pipeline
def udp3_pipeline14():
    delay_5()    

@pipeline
def udp3_pipeline15():
    delay_5()    

@pipeline
def udp3_pipeline16():
    delay_5()    

@pipeline
def udp3_pipeline17():
    delay_5()    

@pipeline
def udp3_pipeline18():
    delay_5()    

@pipeline
def udp3_pipeline19():
    delay_5()    

@pipeline
def udp3_pipeline20():
    delay_5()    

@pipeline
def udp3_pipeline21():
    delay_5()

@pipeline
def udp3_pipeline22():
    delay_5()    

@pipeline
def udp3_pipeline23():
    delay_5()

@pipeline
def udp3_pipeline24():
    delay_5()

@pipeline
def udp3_pipeline25():
    delay_5()

@pipeline
def udp3_pipeline26():
    delay_5()

@pipeline
def udp3_pipeline27():
    delay_5()

@pipeline
def udp3_pipeline28():
    delay_5()

@pipeline
def udp3_pipeline29():
    delay_5()

@pipeline
def udp3_pipeline30():
    delay_5()

@pipeline
def udp3_pipeline31():
    delay_5()    

@pipeline
def udp3_pipeline32():
    delay_5()

@pipeline
def udp3_pipeline33():
    delay_5()    

@pipeline
def udp3_pipeline34():
    delay_5()

@pipeline
def udp3_pipeline35():
    delay_5()

@pipeline
def udp3_pipeline36():
    delay_5()

@pipeline
def udp3_pipeline37():
    delay_5()

@pipeline
def udp3_pipeline38():
    delay_5()

@pipeline
def udp3_pipeline39():
    delay_5()    

@pipeline
def udp3_pipeline40():
    delay_5()    

@pipeline
def udp3_pipeline41():
    delay_5()   

@pipeline
def udp3_pipeline42():
    delay_5()

@pipeline
def udp3_pipeline43():
    delay_5()

@pipeline
def udp3_pipeline44():
    delay_5()

@pipeline
def udp3_pipeline45():
    delay_5()

@pipeline
def udp3_pipeline46():
    delay_5()    

@pipeline
def udp3_pipeline47():
    delay_5()    

@pipeline
def udp3_pipeline48():
    delay_5()    

@pipeline
def udp3_pipeline49():
    delay_5()    

@pipeline
def udp3_pipeline50():
    delay_5()    




def pipelines_with_schedules():
    pipeline_template_name = 'udp3_pipeline'
    pipeline_and_schedules = []
    for pipeline_number in range(1,51):
        pipeline_and_schedules.append(globals()[f'{pipeline_template_name}{pipeline_number}'])
        pipeline_and_schedules.append(ScheduleDefinition(name=f"schedule{pipeline_number}",
                                                        pipeline_name= f"udp3_pipeline{pipeline_number}",
                                                        cron_schedule="*/1 * * * *")
                                                        )
    return pipeline_and_schedules    

@repository
def udp3_repo():
    return pipelines_with_schedules()